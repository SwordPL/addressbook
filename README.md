#Erlang Addressbook Module assignment
##Aim
    Aim for the module is to learn how to create and manage data structures by usage of the functional language as Erlang.
    For more information concerning Erlang language visit [Erlang Organisation webpage](http://www.erlang.org/).

##Functionality
    The Addressbook module should allow to:
     * Create new, empty address book
     * Add contact to the book
     * Add contact's email addresses (home, work)
     * Add contact's phone numbers
     * Remove contact with all associated data
     * Remove only phone number or address
     * Get the list of phone numbers for given name and surname
     * Get the list of email addresses for given name and surname
     * Get the person on the basis of email address
     * Get the person on the basis of phone number

##Installation and usage
    After compilation of the source code, it is to be used as the module. It is not designed to work as application.
    Therefore, usage in Erlang REPL is encouraged.

##Project structure
    The code is monolithic module with exported required interface and some helper functions. In /test/ directory,
    there is monolithic test module.

    No additional modules are required to compile and use module. Tests are written in EUnit. 
