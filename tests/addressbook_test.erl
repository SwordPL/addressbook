%%%-------------------------------------------------------------------
%%% @author Hubert Pomorski
%%% @copyright (C) 2014, AGH University of Science and Technology
%%% @doc
%%% Testing module for AddressBook module.
%%% @end
%%%-------------------------------------------------------------------
-module(addressbook_test).
-author("Hubert Pomorski").
-record(person, {name = "", phone = [], email = []}).

-include_lib("eunit/include/eunit.hrl").

createAddressBook_test() ->
  ?assertEqual(addressbook:createAddressBook(), []).

addContact_normal_test() ->
  AB = addressbook:createAddressBook(),
  ?assertMatch([#person{name = "John Smith"}],
    addressbook:addContact("John", "Smith", AB)).

addContact_abnormal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ?assertError(person_duplicate, addressbook:addContact("John", "Smith", AB)).

getEmails_normal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ?assertEqual([],
    addressbook:getEmails("John", "Smith", AB)).

getEmails_abnormal_test() ->
  AB = addressbook:createAddressBook(),
  ?assertError(person_not_found,
    addressbook:getEmails("John", "Smith", AB)).

addEmail_normal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ABE = addressbook:addEmail("John", "Smith", "js@mail.com", AB),
  ?assertEqual(["js@mail.com"], addressbook:getEmails("John", "Smith", ABE)).

addEmail_double_normal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ABE = addressbook:addEmail("John", "Smith", "js@mail.com", AB),
  ABE2 = addressbook:addEmail("John", "Smith", "smithy@mail.com", ABE),
  ?assertEqual(["smithy@mail.com", "js@mail.com"],
    addressbook:getEmails("John", "Smith", ABE2)).

addEmail_nonexistant_person_test() ->
  AB = addressbook:createAddressBook(),
  ABT = addressbook:addEmail("John", "Smith", "js@mail.com", AB),
  ?assertEqual(["js@mail.com"],
    addressbook:getEmails("John", "Smith", ABT)).

addEmail_duplicate_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ABE = addressbook:addEmail("John", "Smith", "js@mail.com", AB),
  ?assertError(email_duplicate, addressbook:addEmail("John", "Smith", "js@mail.com", ABE)).


getPhones_normal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ?assertEqual([],
    addressbook:getPhones("John", "Smith", AB)).

getPhones_abnormal_test() ->
  AB = addressbook:createAddressBook(),
  ?assertError(person_not_found,
    addressbook:getPhones("John", "Smith", AB)).


addPhone_normal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ABE = addressbook:addPhone("John", "Smith", "(12) 123-93-12", AB),
  ?assertEqual(["(12) 123-93-12"], addressbook:getPhones("John", "Smith", ABE)).

addPhone_double_normal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ABE = addressbook:addPhone("John", "Smith", "(12) 123-93-12", AB),
  ABE2 = addressbook:addPhone("John", "Smith", "505321123", ABE),
  ?assertEqual(["505321123", "(12) 123-93-12"],
    addressbook:getPhones("John", "Smith", ABE2)).

addPhone_nonexistant_person_test() ->
  AB = addressbook:createAddressBook(),
  ABT = addressbook:addPhone("John", "Smith", "(12) 123-93-12", AB),
  ?assertEqual(["(12) 123-93-12"],
    addressbook:getPhones("John", "Smith", ABT)).

addPhone_duplicate_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ABE = addressbook:addPhone("John", "Smith", "(12) 123-93-12", AB),
  ?assertError(phone_duplicate, addressbook:addPhone("John", "Smith", "(12) 123-93-12", ABE)).

removeContact_only_person_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ?assertEqual([], addressbook:removeContact("John", "Smith", AB)).

removeContact_two_person_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  AB1 = addressbook:addContact("John", "Myslovsky", AB),
  ?assertEqual([#person{name = "John Myslovsky"}], addressbook:removeContact("John", "Smith", AB1)).

removeContact_non_existent_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ?assertError(person_not_found, addressbook:removeContact("John", "Myslovsky", AB)).

removePhone_only_Phone_test() ->
  AB = addressbook:addPhone("John", "Smith", "(12) 123-93-12", addressbook:createAddressBook()),
  AB1 = addressbook:removePhone("(12) 123-93-12", AB),
  ?assertEqual([], addressbook:getPhones("John", "Smith", AB1)).

removePhone_two_Phone_test() ->
  AB = addressbook:addPhone("John", "Smith", "(12) 123-93-12", addressbook:createAddressBook()),
  AB1 = addressbook:addPhone("John", "Smith", "505321123", AB),
  AB2 = addressbook:removePhone("(12) 123-93-12", AB1),
  ?assertEqual(["505321123"], addressbook:getPhones("John", "Smith", AB2)).

removePhone_abnormal_test() ->
  AB = addressbook:addContact("John", "Smith", addressbook:createAddressBook()),
  ?assertError(phone_not_found, addressbook:removePhone("smithy@mail.com", AB)).

removeEmail_only_email_test() ->
  AB = addressbook:addEmail("John", "Smith", "js@mail.com", addressbook:createAddressBook()),
  AB1 = addressbook:removeEmail("js@mail.com", AB),
  ?assertEqual([], addressbook:getEmails("John", "Smith", AB1)).

removeEmail_two_email_test() ->
  AB = addressbook:addEmail("John", "Smith", "js@mail.com", addressbook:createAddressBook()),
  AB1 = addressbook:addEmail("John", "Smith", "smithy@mail.com", AB),
  AB2 = addressbook:removeEmail("js@mail.com", AB1),
  ?assertEqual(["smithy@mail.com"], addressbook:getEmails("John", "Smith", AB2)).

removeEmail_abnormal_test() ->
  AB = addressbook:addEmail("John", "Smith", "js@mail.com", addressbook:createAddressBook()),
  ?assertError(email_not_found, addressbook:removeEmail("smithy@mail.com", AB)).
