%%%-------------------------------------------------------------------
%%% @author Hubert Pomorski
%%% @copyright (C) 2014, AGH University of Science and Technology
%%% @doc
%%% The module aims into simulating the addressbook behaviour.
%%% @end
%%%-------------------------------------------------------------------
-module(addressbook).
-author("Hubert Pomorski").
-record(person, {name = "", phone = [], email = []}).
%% API
-export([createAddressBook/0, addContact/3, removeContact/3,
  addPhone/4, addEmail/4, removePhone/2,
  removeEmail/2, getEmails/3, getPhones/3,
  findByPhone/2, findByEmail/2]).

createAddressBook() -> [].

addContact(Name, Surname, AB) ->
  case findPersonRecord(strplus(Name, Surname), AB) of
    person_not_found -> [#person{name = strplus(Name, Surname)} | AB];
    _ -> error(person_duplicate)
  end.

addEmail(Name, Surname, {Category, Email}, AB) ->
  try findByEmail({Category, Email}, AB) of
    _ -> error(email_duplicate)
  catch
    error:email_not_found ->
      addEmailToList(strplus(Name, Surname), {Category, Email}, AB)
  end.

addEmailToList(NameS, {Category, Email}, []) -> [#person{name = NameS, email = [{Category, Email}]}];
addEmailToList(NameS, {Category, Email}, [H = #person{name = NameS} | T]) ->
  [#person{name = NameS, phone = H#person.phone, email = [{Category, Email}] ++ H#person.email} | T];
addEmailToList(NameS, {Category, Email}, [_ | T]) -> addEmailToList(NameS, {Category, Email}, T).

addPhone(Name, Surname, Phone, AB) ->
  try findByPhone(Phone, AB) of
    _ -> error(phone_duplicate)
  catch
    error:phone_not_found ->
      addPhoneToList(strplus(Name, Surname), Phone, AB)
  end.

addPhoneToList(NameS, Phone, []) -> [#person{name = NameS, phone = [Phone]}];
addPhoneToList(NameS, Phone, [H = #person{name = NameS} | T]) ->
  [#person{name = NameS, phone = [Phone] ++ H#person.phone, email = H#person.email} | T];
addPhoneToList(NameS, Phone, [_ | T]) -> addPhoneToList(NameS, Phone, T).

%Read
getEmails(Name, Surname, AB) ->
  case findPersonRecord(strplus(Name, Surname), AB) of
    person_not_found -> error(person_not_found);
    #person{email = Email} -> Email
  end.

getPhones(Name, Surname, AB) ->
  case findPersonRecord(strplus(Name, Surname), AB) of
    person_not_found -> error(person_not_found);
    #person{phone = Phone} -> Phone
  end.

findByEmail(_, []) -> error(email_not_found);
findByEmail({Category, Email}, [H | T]) ->
  case findDataForUser({Category, Email}, H#person.email) of
    true -> H#person.name;
    false -> findByEmail({Category, Email}, T)
  end.

findByPhone(_, []) -> error(phone_not_found);
findByPhone(Phone, [H | T]) ->
  case findDataForUser(Phone, H#person.phone) of
    true -> H#person.name;
    false -> findByPhone(Phone, T)
  end.

%Destroy
removeContact(Name, Surname, AB) ->
  case findPersonRecord(strplus(Name, Surname), AB) of
    person_not_found -> error(person_not_found);
    _ -> doRemoveContact(strplus(Name, Surname), AB)
  end.

doRemoveContact(_, []) -> error(person_not_found);
doRemoveContact(Name, [#person{name = Name} | T]) -> T;
doRemoveContact(Name, [H | T]) -> [H | doRemoveContact(Name, T)].

removeEmail(Email, AB) ->
  case emailExists(Email, AB) of
    true -> doRemoveEmail(Email, AB);
    false -> error(email_not_found)
  end.

emailExists(_, []) -> false;
emailExists({Category, Email}, [H | T]) ->
  case findDataForUser({Category, Email}, H#person.email) of
    true -> true;
    _ -> emailExists({Category, Email}, T)
  end.

doRemoveEmail(_, []) -> [];
doRemoveEmail({Category, Email}, [H | T]) -> [#person
{name = H#person.name,
  phone = H#person.phone,
  email = [X || X <- H#person.email, X /= {Category, Email}]}
  | doRemoveEmail({Category, Email}, T)].

removePhone(Phone, AB) ->
  case phoneExists(Phone, AB) of
    true -> doRemovePhone(Phone, AB);
    false -> error(phone_not_found)
  end.

phoneExists(_, []) -> false;
phoneExists(Phone, [H | T]) ->
  case findDataForUser(Phone, H#person.phone) of
    true -> true;
    _ -> phoneExists(Phone, T)
  end.

doRemovePhone(_, []) -> [];
doRemovePhone(Phone, [H | T]) -> [#person
{name = H#person.name,
  phone = [X || X <- H#person.phone, X /= Phone],
  email = H#person.email}
  | doRemovePhone(Phone, T)].

%%%Helper methods
findPersonRecord(_, []) -> person_not_found;
findPersonRecord(NameS, [X1 = #person{name = NameS} | _]) -> X1;
findPersonRecord(NameS, [_ | T]) -> findPersonRecord(NameS, T).

findDataForUser(_, []) -> false;
findDataForUser(Data, [Data | _]) -> true;
findDataForUser({Data1, Data2}, [{Data1, Data2} | _]) -> true;
findDataForUser(Data, [_ | T]) -> findDataForUser(Data, T);
findDataForUser({Data1, Data2}, [_ | T]) -> findDataForUser({Data1, Data2}, T).

strplus(Name, Surname) -> Name ++ " " ++ Surname.